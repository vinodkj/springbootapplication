package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.module.Topics;

public interface TopicRepository extends JpaRepository<Topics, String>
{

}
