package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.module.Author;

public interface AuthorRespository  extends JpaRepository<Author, String>
{


}
