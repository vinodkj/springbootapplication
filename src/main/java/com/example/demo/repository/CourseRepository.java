package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.module.Courses;

public interface CourseRepository extends JpaRepository<Courses, String>
{

}
