package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.module.BankAccount;

public interface AccountRepository extends JpaRepository<BankAccount, Long> {
	//public BankAccount findByAccNo(Long accno);

}
