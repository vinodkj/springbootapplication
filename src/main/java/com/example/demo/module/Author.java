package com.example.demo.module;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

//import org.hibernate.annotations.ManyToAny;

@Entity
@Table(name="author")
public class Author 
{
	@Id
	private String auid;
	@Column
	private String name;
	@Column
	private String email;
	@ManyToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	@JoinTable(name="authorid")
	private List<Author> book;
	
	
	
	public List<Author> getBook() {
		return book;
	}
	public void setBook(List<Author> book) {
		this.book = book;
	}
	public String getAuid() {
		return auid;
	}
	public void setAuid(String auid) {
		this.auid = auid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

}
