package com.example.demo.module;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="store")
public class StoreDetails 
{
	@Id
    private String gstn;
	
	@NotNull(message = "store name should not be same")
	private String storeName;
	@NotNull
	private String ownerDetails;
	@Size(max = 1000)
	private String address;
	public String getGstn() {
		return gstn;
	}
	public void setGstn(String gstn) {
		this.gstn = gstn;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public String getOwnerDetails() {
		return ownerDetails;
	}
	public void setOwnerDetails(String ownerDetails) {
		this.ownerDetails = ownerDetails;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
}
