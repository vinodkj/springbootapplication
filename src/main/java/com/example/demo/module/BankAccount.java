package com.example.demo.module;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "accountdetails")
@SequenceGenerator(name = "seq", initialValue = 20000000, allocationSize = 100)
public class BankAccount {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long accNo;
	private String ifssCode;
	private double balance;

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public Long getAccNo() {
		return accNo;
	}

	public void setAccNo(Long accNo) {
		this.accNo = accNo;
	}

	public String getIfssCode() {
		return ifssCode;
	}

	public void setIfssCode(String ifssCode) {
		this.ifssCode = ifssCode;
	}

	

}
