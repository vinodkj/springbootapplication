package com.example.demo.module;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
//import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="courses")
public class Courses implements Serializable
{
	@Id
	private String courseId;
	
	private String courseName;
	private String descreption;
	
	
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	private List<Topics> topic;
	
	public void setTopic(List<Topics> topic) {
		this.topic = topic;
	}

	public List<Topics> getTopic() {
		return topic;
	}
	
	public String getCourseId() {
		return courseId;
	}
	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getDescreption() {
		return descreption;
	}
	public void setDescreption(String descreption) {
		this.descreption = descreption;
	}
	
	
}
