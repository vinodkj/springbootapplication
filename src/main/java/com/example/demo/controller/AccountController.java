package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.module.BankAccount;
import com.example.demo.service.AccountService;

@RestController
public class AccountController {
	@Autowired
	private AccountService accountService;

	@GetMapping(value = "details")
	public ResponseEntity<List<BankAccount>> getAccountDetails() {

		List<BankAccount> acc = accountService.getAccountDetails();
		return ResponseEntity.status(200).body(acc);
	}
	@PostMapping(value="details")
	public ResponseEntity<BankAccount> saveAccountDetails(@RequestBody BankAccount accontDetails)
	{
		return ResponseEntity.status(201).body(accountService.addAccount(accontDetails));
	}
	@PutMapping(value="details/{accNo}")
	public ResponseEntity<BankAccount> updateBalance(@RequestBody BankAccount account,@PathVariable("accNo") int accNo)
	{
		
		
		return ResponseEntity.status(200).body(accountService.updateAccount(account, accNo));
	}

}
