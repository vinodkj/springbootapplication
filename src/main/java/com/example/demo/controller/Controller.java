package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.module.Courses;
import com.example.demo.service.Service;

@RestController
public class Controller
{
	@Autowired
	private Service service;
	
	
	@PostMapping(value="/addcourse")
	public ResponseEntity< Courses> saveCourse(@RequestBody Courses course)
	{
		Courses cou=service.saveCouse(course);
		return ResponseEntity.status(200).body(cou);
	}
	@GetMapping(value="/courses")
	public ResponseEntity<List<Courses>> getAllCourses()
	{
	  List<Courses> courses=service.getAllCourses();
	  return ResponseEntity.status(201).body(courses);
		
	}

}
