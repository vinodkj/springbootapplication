package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.module.Product;
import com.example.demo.service.ProductService;

@RestController
public class ProductController {
	List<Product> li = new ArrayList<Product>();

	@Autowired
	private ProductService productService;

	@GetMapping(value = "products")
	public ResponseEntity<List<Product>> getAllProduct() {
		li = productService.getAllProducts();
		return ResponseEntity.status(200).body(li);
	}

	@PostMapping(value = "products")
	public ResponseEntity<Product> saveProduct(@RequestBody Product product) {
		return ResponseEntity.status(201).body(productService.saveProduct(product));
	}

	@PutMapping(value = "products/{pid}")
	public ResponseEntity<Product> updateProduct(@RequestBody Product product, @PathVariable("pid") int pid) 
	{
		product.setPid(pid);
		Product pro=productService.updatePrice(product);
		
		return ResponseEntity.status(201).body(pro);
	}

}
