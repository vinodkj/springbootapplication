package com.example.demo.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.module.Person;
import com.example.demo.service.PersonService;

@RestController
public class PersonController 
{
	@Autowired
	private PersonService personService;
	
	
	@GetMapping(value="/persons", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Person>> getAllStduents()
	{
		List<Person> person=personService.getAllPerson();
		return ResponseEntity.status(200).body(person);
	}
	@PostMapping(value="persons",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Person> savePersonDetails(@Valid @RequestBody Person person)
	{
		return ResponseEntity.status(201).body(personService.addPerson(person));
	}
	@PutMapping(value="/persons/{id}")
	public ResponseEntity<Person> updatePersonDetails(@Valid @RequestBody Person person,@PathVariable("id")int id)
	{
		   person.setId(id);
		return ResponseEntity.status(201).body(personService.updatePersonDetails(person));
	}
	
}
