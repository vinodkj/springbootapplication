package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.example.demo.module.Product;
import com.example.demo.repository.ProductRepository;

@Service
public class ProductService
{
	List<Product> list=new ArrayList<Product>();
	@Autowired
	private ProductRepository productRepository;
	
	
	public Product saveProduct(Product product)
	{
		return productRepository.save(product);
	}
	
	@CachePut(value = "mycache", key="#po.pid")
	public Product updatePrice(Product po)
	{
		  Product pro=productRepository.saveAndFlush(po);
          return pro;
	}
	
	@Cacheable(cacheNames = "product")
	public List<Product> getAllProducts()
	{
		list=productRepository.findAll();
		return list;
	}
	
	
	

}
