package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.example.demo.module.Person;
import com.example.demo.repository.PersonRepository;

@Service
public class PersonService 
{
	@Autowired
	private PersonRepository personRepository;
	
	public Person addPerson(Person person)
	{
		return personRepository.save(person);
	}
	@Cacheable(cacheNames = "mycache")
	public List<Person> getAllPerson()
	{
		return personRepository.findAll();
	}
	
	@CachePut( value = "personCache",key="#id")
	public Person updatePersonDetails(Person person)
	{
		return personRepository.saveAndFlush(person);
		
}
	
	
	

}
