package com.example.demo.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.module.Courses;
import com.example.demo.module.Topics;
import com.example.demo.repository.CourseRepository;
import com.example.demo.repository.TopicRepository;
@org.springframework.stereotype.Service
public class Service
{
	@Autowired
	private CourseRepository courseRepository;
	
	@Autowired
	private TopicRepository topicRepository;
	
	public Courses saveCouse(Courses course)
	{
		return courseRepository.save(course);
		
	}
	public List<Courses> getAllCourses()
	{
		return courseRepository.findAll();
	
	}
	public Topics saveTopic(Topics topic)
	{
		return topicRepository.save(topic);
		
	}
	public List<Topics> getAllTopics()
	{
		return topicRepository.findAll();
	
	}
	
	

}
