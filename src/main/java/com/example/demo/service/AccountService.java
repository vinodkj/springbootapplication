package com.example.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.module.BankAccount;
import com.example.demo.repository.AccountRepository;

@Service
public class AccountService {
	@Autowired
	private AccountRepository accountRepository;

	public BankAccount addAccount(BankAccount account) {

		return accountRepository.save(account);
	}
     @Transactional(rollbackOn = Exception.class)
	public BankAccount updateAccount(BankAccount account, int balance) 
     {
    	 int a=10;
		account.setBalance(balance);
	     int b=a/10;
		return accountRepository.saveAndFlush(account);

    }

	public List<BankAccount> getAccountDetails() 
	{
		List<BankAccount>  acc=accountRepository.findAll();
		return acc;
	}

}
