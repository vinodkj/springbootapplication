package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.example.demo.module.StoreDetails;
import com.example.demo.repository.StoreDetailsRepository;

@Service
public class StoreService
{
	@Autowired
	private StoreDetailsRepository storeDetailsRepository;
	
	@Async
	public StoreDetails saveStoreDetails(StoreDetails storeDetails)
	{
		StoreDetails details=storeDetailsRepository.save(storeDetails);
		try
		{
			Thread.sleep(20);
		}catch(InterruptedException o)
		{
			o.printStackTrace();
		}
		
		    return details ;
	}
	
	@Cacheable(cacheNames = "stroredetails")
	public List<StoreDetails> getAllStoreDetails()
	{
		List<StoreDetails> list=storeDetailsRepository.findAll();
		return list;
	}
	
	@CachePut(cacheNames = "storedetails", key = "#id")
	@Async
       public StoreDetails updateStroreDetails(StoreDetails store)
	{
		return storeDetailsRepository.saveAndFlush(store);
	}
	
	

}
